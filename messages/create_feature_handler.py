from microservice_system_engine.abstract_classes import AbstractMessageHandler, AbstractCachingSystem
from microservice_system_engine.messaging import CreateFeatureRequest
from microservice_system_engine.utils.feature_data_cache import FeatureDataCache


class CreateFeatureHandler(AbstractMessageHandler):
    __caching_system: AbstractCachingSystem = None

    @staticmethod
    def assign_caching_system(caching_system: AbstractCachingSystem):
        CreateFeatureHandler.__caching_system = caching_system

    @staticmethod
    def handle(json_message: dict) -> None:
        message = CreateFeatureRequest.create_instance(json_message)
        if CreateFeatureHandler.__caching_system.get(message.feature_data_cache.feature_data.to_string()):
            # TODO: log
            return
        CreateFeatureHandler.__caching_system.put(message.feature_data_cache.feature_data.to_string(),
                                                    FeatureDataCache(message.feature_data_cache.feature_data,
                                                                     message.feature_data_cache.destination,
                                                                     [dependency for dependency in message.feature_data_cache.dependencies]).to_json())
