from microservice_system_engine.abstract_classes import AbstractMessageHandler, AbstractMessageManager, AbstractCachingSystem
from microservice_system_engine.messaging import FeaturesResponse, FeaturesRequest
from utils.execution_plan import ExecutionPlan
from typing import List
from utils.execution_level import ExecutionLevel
from utils.utils import from_feature_data_cache_to_alias


class FeatureResponseHandler(AbstractMessageHandler):
    __message_manager: AbstractMessageManager = None
    __caching_system: AbstractCachingSystem = None

    @staticmethod
    def assign_message_manager(message_manager: AbstractMessageManager):
        FeatureResponseHandler.__message_manager = message_manager

    @staticmethod
    def assign_caching_system(caching_system: AbstractCachingSystem):
        FeatureResponseHandler.__caching_system = caching_system

    @staticmethod
    def handle(json_message: dict) -> None:
        message = FeaturesResponse.create_instance(json_message)
        cache_data_json = FeatureResponseHandler.__caching_system.get(message.request_id)
        if cache_data_json is None:
            # TODO: log
            return
        execution_plan = ExecutionPlan.from_json(cache_data_json)
        execution_plan.data, execution_plan.levels[execution_plan.current_level].columns = FeatureResponseHandler.__update_data(execution_plan.data,
                                                                                                                                message.data,
                                                                                                                                message.generated_features)

        if not execution_plan.is_time_to_change_level():
            FeatureResponseHandler.__caching_system.delete(execution_plan.request_id)
            FeatureResponseHandler.__caching_system.put(execution_plan.request_id, execution_plan.to_json())
            return

        execution_plan.current_level += 1
        if len(execution_plan.levels) > execution_plan.current_level:
            FeatureResponseHandler.__send_requests_to_everyone_one_level(message.request_id,
                                                                         execution_plan.data,
                                                                         execution_plan.levels[execution_plan.current_level])
            FeatureResponseHandler.__caching_system.put(execution_plan.request_id, execution_plan.to_json())
        else:
            response = FeaturesResponse(execution_plan.request_id, 'coordinator', [], execution_plan.data)
            FeatureResponseHandler.__message_manager.send_message(execution_plan.callback_destination, response)
            FeatureResponseHandler.__caching_system.delete(execution_plan.request_id)


    @staticmethod
    def __send_requests_to_everyone_one_level(request_id: str, data, level: ExecutionLevel):
        for transformer_name in level.transformers.keys():


            FeatureResponseHandler.__message_manager.send_message(
                level.transformers[transformer_name][0].destination,
                # TODO: rewrite to have destination as a map key instead of transformer name
                FeaturesRequest(request_id,
                                [from_feature_data_cache_to_alias(transformer_name, level.transformers[transformer_name])],
                                data,
                                FeatureResponseHandler.__message_manager.get_listening_destination()))


    @staticmethod
    def __update_data(old_data, new_data, features: List[str]):
        for key in new_data:
            if key not in old_data:
                old_data[key] = new_data[key]
                if key in features:
                    features.remove(key)
                else:
                    # TODO: log
                    continue
        return old_data, features