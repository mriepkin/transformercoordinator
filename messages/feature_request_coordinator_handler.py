from microservice_system_engine.abstract_classes import AbstractMessageHandler, AbstractMessageManager, AbstractCachingSystem
from microservice_system_engine.messaging import FeaturesRequestCoordinator, FeaturesRequest
from microservice_system_engine.utils import FeatureData
from microservice_system_engine.utils import FeatureDataCache
from utils.execution_level import ExecutionLevel
from utils.execution_plan import ExecutionPlan
from typing import List, Dict
from utils.utils import from_feature_data_cache_to_alias


#TODO: handle if features depend on features from the same transformer
class FeatureRequestCoordinatorHandler(AbstractMessageHandler):
    __message_manager: AbstractMessageManager = None
    __caching_system: AbstractCachingSystem = None

    @staticmethod
    def assign_message_manager(message_manager: AbstractMessageManager):
        FeatureRequestCoordinatorHandler.__message_manager = message_manager

    @staticmethod
    def assign_caching_system(caching_system: AbstractCachingSystem):
        FeatureRequestCoordinatorHandler.__caching_system = caching_system

    @staticmethod
    def handle(json_message: dict) -> None:
        message = FeaturesRequestCoordinator.create_instance(json_message)
        cache_data_json = FeatureRequestCoordinatorHandler.__caching_system.get(message.request_id)
        if cache_data_json is None:
            dependencies = FeatureRequestCoordinatorHandler.__get_dependencies(message.features)
            execution_levels = FeatureRequestCoordinatorHandler.__topology_sort(dependencies)
            FeatureRequestCoordinatorHandler.__caching_system.put(message.request_id, ExecutionPlan(message.request_id,
                                                                                                    execution_levels,
                                                                                                    0,
                                                                                                    message.data,
                                                                                                    message.callback_topic).to_json())
            if len(execution_levels) == 0:
                return
            FeatureRequestCoordinatorHandler.__send_requests_to_everyone_one_level(message.request_id, message.data, execution_levels[0])
        else:
            # TODO: in case of splitter
            return

    @staticmethod
    def __send_requests_to_everyone_one_level(request_id: str, data, level: ExecutionLevel):
        for transformer_name in level.transformers.keys():
            FeatureRequestCoordinatorHandler.__message_manager.send_message(
                level.transformers[transformer_name][0].destination,
                # TODO: rewrite to have destination as a map key instead of transformer name
                FeaturesRequest(request_id,
                                [from_feature_data_cache_to_alias(transformer_name, level.transformers[transformer_name])],
                                data,
                                FeatureRequestCoordinatorHandler.__message_manager.get_listening_destination()))

    @staticmethod
    def __get_dependencies(required_columns: List[FeatureData]) -> Dict[str, List[FeatureDataCache]]:
        # {transformer_name: [FeatureDataCache]}
        result_columns: Dict[str, List[FeatureDataCache]] = {}
        for col in required_columns:
            feature_json = FeatureRequestCoordinatorHandler.__caching_system.get(col.to_string())
            if feature_json is None:
                #TODO: log
                return None
            feature = FeatureDataCache.from_json(feature_json)
            result_columns = FeatureRequestCoordinatorHandler.__do_get_dependencies(feature, result_columns)
        return result_columns

    @staticmethod
    def __topology_sort(columns: Dict[str, List[FeatureDataCache]]) -> List[ExecutionLevel]:
        result_columns: List[ExecutionLevel] = []
        return FeatureRequestCoordinatorHandler.__do_topology_sort(result_columns, columns)

    @staticmethod
    def __do_topology_sort(result_columns: List[ExecutionLevel], columns: Dict[str, List[FeatureDataCache]]) -> List[ExecutionLevel]:
        level = 0
        while len(columns.keys()) != 0:
            keys_to_remove = []
            for transformer_name in columns.keys():
                for column in columns[transformer_name]:
                    if (level == 0 and len(column.dependencies) == 0) or \
                            (level > 0 and all(elem in result_columns[level - 1].columns for elem in column.dependencies)):
                        if len(result_columns) <= level:
                                result_columns.append(ExecutionLevel([], {}))
                        if column.feature_data.transformer_name not in result_columns[level].transformers.keys():
                            result_columns[level].transformers[column.feature_data.transformer_name] = []
                        result_columns[level].transformers[column.feature_data.transformer_name].append(column)
                        result_columns[level].columns.append(column.feature_data.to_string())
                        columns[transformer_name].remove(column)
                if len(columns[transformer_name]) == 0:
                    keys_to_remove.append(transformer_name)
            for key in keys_to_remove:
                columns.pop(key)

            level += 1
        return result_columns

    @staticmethod
    def __do_get_dependencies(feature: FeatureDataCache, result: Dict[str, List[FeatureDataCache]]):
        FeatureRequestCoordinatorHandler.__insert_feature(result, feature)

        for col in feature.dependencies:
            dependent_feature_json = FeatureRequestCoordinatorHandler.__caching_system.get(col)
            dependent_feature = FeatureDataCache.from_json(dependent_feature_json)
            result = FeatureRequestCoordinatorHandler.__do_get_dependencies(dependent_feature, result)
        return result

    @staticmethod
    def __insert_feature(result: Dict[str, List[FeatureDataCache]], feature: FeatureDataCache):
        if feature.feature_data.transformer_name not in result:
            result[feature.feature_data.transformer_name] = []
        result[feature.feature_data.transformer_name].append(feature)
        return result
