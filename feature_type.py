from enum import Enum


class FeatureType(Enum):
    INVALID = 0
    CATEGORICAL = 1
    NUMERIC = 2
    NEARLY_ID = 3