# cache
from microservice_system_engine.configs import CacheSystemConfig
from microservice_system_engine.caching import CachingSystem

# messaging system
from microservice_system_engine.configs import MessageSystemConfig
from microservice_system_engine.messaging import MessageSystem

# db
from microservice_system_engine.configs import DatabaseConfig
from microservice_system_engine.databases import DbConnector

# logs
from microservice_system_engine.configs import ConsoleLoggerConfig
from microservice_system_engine.loggers import ConsoleLogger
from microservice_system_engine.loggers import LoggerManager

# message manager
from microservice_system_engine.messaging import MessageManager

# message handlers
from messages.feature_request_coordinator_handler import FeatureRequestCoordinatorHandler
from messages.feature_response_handler import FeatureResponseHandler
from messages.create_feature_handler import CreateFeatureHandler

from microservice_system_engine.messaging import MessageHandlerManager
from microservice_system_engine.utils.message_codes import MessageCodes

from microservice_system_engine.messaging.features_messages import FeatureData, FeaturesRequest

from typing import List

import json



class Handler:
    __message_handler: MessageHandlerManager = None
    __message_manager: MessageManager = None
    __cache_system: CachingSystem = None
    __message_system: MessageSystem = None


    @staticmethod
    def __read_json(path: str):
        with open(path, 'r') as f:
            return json.loads(''.join(f.readlines()))

    @staticmethod
    def __init_cache(path_to_config: str):
        json_config = Handler.__read_json(path_to_config)
        config = CacheSystemConfig(json_config['host'], json_config['port'])
        return CachingSystem(config)

    @staticmethod
    def __init_message_system(path_to_config: str):
        json_config = Handler.__read_json(path_to_config)
        config = MessageSystemConfig(MessageSystemConfig.Producer(json_config['producer']['server'], lambda x: json.dumps(x).encode('utf-8')),
                                     MessageSystemConfig.Consumer(json_config['consumer']['topic'], json_config['consumer']['server'], lambda x: json.loads(x.decode('utf-8')), auto_offset_reset=json_config['consumer']['auto_offset_reset']))
        return MessageSystem(config)

    @staticmethod
    def __init_db(path_to_config: str):
        json_config = Handler.__read_json(path_to_config)
        config = DatabaseConfig(json_config['username'],
                                json_config['password'],
                                json_config['cluster_host'],
                                json_config['port'],
                                json_config['key_space'])
        return DbConnector(config)

    @staticmethod
    def __init_local_console_logger(path_to_config: str):
        json_config = Handler.__read_json(path_to_config)
        config = ConsoleLoggerConfig(json_config['min_log_level'])
        return ConsoleLogger(config)

    @staticmethod
    def initialize():
        Handler.__cache_system = Handler.__init_cache('./configs/cache_config.json')
        Handler.__message_system = Handler.__init_message_system('./configs/messaging_config.json')
        console_logger = Handler.__init_local_console_logger('./configs/console_logger_config.json')
        LoggerManager.add_logger(console_logger)
        Handler.__message_manager = MessageManager(LoggerManager, Handler.__message_system)
        Handler.__message_handler = MessageHandlerManager(Handler.__message_manager)
        FeatureRequestCoordinatorHandler.assign_message_manager(Handler.__message_manager)
        FeatureRequestCoordinatorHandler.assign_caching_system(Handler.__cache_system)
        FeatureResponseHandler.assign_message_manager(Handler.__message_manager)
        FeatureResponseHandler.assign_caching_system(Handler.__cache_system)
        CreateFeatureHandler.assign_caching_system(Handler.__cache_system)
        Handler.__message_handler.add_handler(MessageCodes.CREATE_FEATURE_REQUEST, CreateFeatureHandler)
        Handler.__message_handler.add_handler(MessageCodes.FEATURES_REQUEST_COORDINATOR, FeatureRequestCoordinatorHandler)
        Handler.__message_handler.add_handler(MessageCodes.FEATURES_RESPONSE, FeatureResponseHandler)


    @staticmethod
    def stop():
        Handler.__cache_system.close_connections()
        Handler.__message_system.close_connections()

    @staticmethod
    def handle_messages():
        for i in range(10):
            Handler.__message_manager.consume_messages()
            Handler.__message_handler.handle_messages()
            Handler.__message_manager.flush_messages()



if __name__ == '__main__':
    Handler.initialize()
    Handler.handle_messages()
    Handler.stop()
