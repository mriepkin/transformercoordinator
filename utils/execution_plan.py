from utils.execution_level import ExecutionLevel
from typing import List
import pandas as pd


class ExecutionPlan:
    def __init__(self, request_id: str, levels: List[ExecutionLevel], current_level: int, data: pd.DataFrame, callback_destination: str):
        self.request_id = request_id
        self.levels = levels
        self.current_level = current_level
        self.data = data
        self.callback_destination = callback_destination

    @staticmethod
    def from_json(raw):
        return ExecutionPlan(
            raw['request_id'],
            [ExecutionLevel.from_json(level) for level in raw['levels']],
            raw['current_level'],
            pd.DataFrame.from_dict(raw['data']),
            raw['callback_destination']
        )

    def to_json(self) -> dict:
        return {
            'request_id': self.request_id,
            'levels': [level.to_json() for level in self.levels],
            'current_level': self.current_level,
            'data': self.data.to_dict(),
            'callback_destination': self.callback_destination
        }

    def is_time_to_change_level(self):
        return len(self.levels[self.current_level].columns) == 0