from typing import List
from microservice_system_engine.utils import Alias, FeatureAlias, FeatureVersionAlias, FeatureImplementationAlias
from microservice_system_engine.utils import FeatureDataCache


def from_feature_data_cache_to_alias(transformer_name: str, features_data_cache: List[FeatureDataCache]) -> Alias:
    grouped_features = __get_same_features(features_data_cache)
    feature_alias: List[FeatureAlias] = []
    for feature in grouped_features:
        feature_name = ''
        feature_implementation_alias = []
        for feature_implementations in feature:
            feature_version_alias: List[List[FeatureVersionAlias]] = []
            implementation = 1
            for i, feature_version in enumerate(feature_implementations):
                feature_name = transformer_name + '_' + feature_version.feature_data.feature_name
                feature_version_alias.append([FeatureVersionAlias(dependency.split('_')[0] + '_' + dependency.split('_')[1], dependency) for dependency in feature_version.dependencies])
                feature_version_alias[i].append(FeatureVersionAlias(feature_name, feature_version.feature_data.to_string()))
                implementation = feature_version.feature_data.feature_version.implementation

            feature_implementation_alias.append(FeatureImplementationAlias(implementation, feature_version_alias))
        feature_alias.append(FeatureAlias(feature_name, feature_implementation_alias))
    return Alias(transformer_name, feature_alias)


def __get_same_features(features_data_cache: List[FeatureDataCache]) -> List[List[List[FeatureDataCache]]]:
    grouped_features: List[List[FeatureDataCache]] = []
    for i, feature in enumerate(features_data_cache):
        grouped_features.append([])
        for other_feature in features_data_cache:
            if feature.feature_data.feature_name == other_feature.feature_data.feature_name:
                grouped_features[i].append(other_feature)
                features_data_cache.remove(other_feature)
    result: List[List[List[FeatureDataCache]]] = []
    for feature_group in grouped_features:
        grouped_by_implementation = __get_same_feature_with_same_implementation(feature_group)
        result.append(grouped_by_implementation)
    return result


def __get_same_feature_with_same_implementation(features_data_cache: List[FeatureDataCache]) -> List[List[FeatureDataCache]]:
    result = []
    features_data_cache.sort(key=lambda c: c.feature_data.feature_version.implementation)
    i = 0
    current_feature_implementation = None
    for feature in features_data_cache:
        if current_feature_implementation is None:
            result.append([])
            current_feature_implementation = feature.feature_data.feature_version.implementation
        if current_feature_implementation > feature.feature_data.feature_version.implementation:
            result.append([])
            i += 1
        result[i].append(feature)
    return result
