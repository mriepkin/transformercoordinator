from typing import List, Dict
from microservice_system_engine.utils.feature_data_cache import FeatureDataCache


class ExecutionLevel:
    def __init__(self, columns: List[str], transformers: Dict[str, List[FeatureDataCache]]):
        self.columns = columns
        self.transformers = transformers

    @staticmethod
    def from_json(raw):
        transformers: Dict[str, List[FeatureDataCache]] = {}
        for transformer_name in raw['transformers'].keys():
            if transformer_name not in transformers:
                transformers[transformer_name] = []
            transformers[transformer_name] = [FeatureDataCache.from_json(feature) for feature in raw['transformers'][transformer_name]]
        return ExecutionLevel(raw['columns'], transformers)

    def to_json(self):
        transformers = {}
        for transformer_name in self.transformers.keys():
            transformers[transformer_name] = [feature_data_cache.to_json() for feature_data_cache in self.transformers[transformer_name]]
        return {
            'columns': self.columns,
            'transformers': transformers
        }